var innerJoin = from ET in EmployeeTeams
		join T in Team on ET.teamId equals T.Id
		join E in Employee on E.Id equals ET.employeeId
		Select new 
		{
		EmployeeFullname = E.fullname,
		EmployeeEmail = E.mail,
		TeamName = T.name,
		TeamCode = T.code
		};
Console.WriteLine("EmployeeFullname\t EmployeeEmail\t TeamName\t TeamCode");
foreach (var data in innerJoin)
{
 Console.WriteLine(data.EmployeeFullname + "\t\t" + data.EmployeeEmail + "\t\t"  + data.TeamName + "\t\t" + data.TeamCode);
}


//Left Outer Join
var leftOuterJoin = from ET in EmployeeTeams
		    join T in Team on E.teamId equals T.Id into eGroup
		    from team in eGroup.DefaultIfEmpty()
		    join E in Employee on E.Id equals ET.employeeId into employ
		    from employee in employ.DefaultIfEmpty()
		    select new
		    {
		     EmployeeFullname = E.fullname,
		     EmployeeEmail = E.mail,
		     TeamName = T.name,
		     TeamCode = T.code
		    };
Console.WriteLine("EmployeeFullname\t EmployeeEmail\t TeamName\t TeamCode");
foreach (var data in leftOuterJoin)
{
 Console.WriteLine(data.EmployeeFullname + "\t\t" + data.EmployeeEmail + "\t\t"  + data.TeamName + "\t\t" + data.TeamCode);
}


//Union 
var union = employee.Union(team)
foreach (var data in union)
{
 Console.WriteLine(data);
}

//intersect
var intersect = employee.Intersect (team)
foreach (var data in intersect )
{
 Console.WriteLine(data);
}